import React from "react";
import { View, FlatList, StyleSheet, Text } from "react-native";
import CustomRow from "./CustomRow";

const CustomListview = ({
  itemList,
  navigation,
  getFlickrPictures,
  setPage,
}) => {
  return (
    <View style={styles.container}>
      <FlatList
        data={itemList}
        renderItem={({ item }) => (
          <CustomRow
            title={item.title}
            description={item.description}
            image_url={item.media.m}
            flickr_url={item.link}
            date_taken={item.date_taken}
            author={item.author}
            navigation={navigation}
          />
        )}
        onEndReachedThreshold={2}
        onEndReached={() => {
          setPage("2");
        }}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
});

export default CustomListview;
