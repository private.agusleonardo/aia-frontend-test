import moment from "moment";
import React from "react";
import { StyleSheet, Text, View, Dimensions, SafeAreaView } from "react-native";
import { ScrollView } from "react-native-gesture-handler";
import Image from "react-native-scalable-image";

function DetailScreen({ route, navigation }) {
  const {
    title,
    description,
    image_url,
    flickr_url,
    date_taken,
    author,
  } = route.params;
  var date = moment(date_taken);
  return (
    <SafeAreaView style={styles.container}>
      <ScrollView>
        <Image
          source={{ uri: image_url }}
          width={Dimensions.get("window").width}
        />
        <View style={styles.container_text}>
          <Text style={styles.title}>{title}</Text>
          <Text style={styles.description}>{description}</Text>
        </View>
        <View style={styles.container_text}>
          <Text>Author : {author}</Text>
          <Text>Taken on : {date.format("MMMM DD, YYYY")}</Text>
          <Text
            style={styles.text_url}
            onPress={() =>
              navigation.push("WebView", {
                flickr_url: flickr_url,
              })
            }
          >
            Visit Flickr
          </Text>
        </View>
      </ScrollView>
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    width: "100%",
  },
  title: {
    fontSize: 20,
    color: "#000",
  },
  container_text: {
    marginTop: 8,
    marginBottom: 16,
    marginLeft: 16,
    marginRight: 16,
    justifyContent: "center",
  },
  description: {
    fontSize: 11,
    fontStyle: "italic",
  },
  photo: {
    height: 200,
    width: "100%",
  },
  text_url: {
    color: "skyblue",
  },
});

export default DetailScreen;
