import React from "react";
import {
  View,
  Text,
  StyleSheet,
  Image,
  TouchableHighlight,
} from "react-native";

const CustomRow = ({
  title,
  description,
  image_url,
  flickr_url,
  date_taken,
  author,
  navigation,
}) => {
  return (
    <View style={styles.container}>
      <TouchableHighlight
        onPress={() =>
          navigation.push("Detail", {
            title: title,
            description: description,
            image_url: image_url,
            flickr_url: flickr_url,
            date_taken: date_taken,
            author: author,
          })
        }
      >
        <View>
          <Image source={{ uri: image_url }} style={styles.photo} />
          <View style={styles.container_text}>
            <Text style={styles.title} numberOfLines={1}>
              {title}
            </Text>
            <Text style={styles.description} numberOfLines={6}>
              {description}
            </Text>
          </View>
        </View>
      </TouchableHighlight>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginLeft: 16,
    marginRight: 16,
    marginTop: 8,
    marginBottom: 8,
    borderRadius: 8,
    backgroundColor: "#FFF",
    elevation: 4,
    overflow: "hidden",
  },
  title: {
    fontSize: 20,
    color: "#000",
  },
  container_text: {
    marginTop: 8,
    marginBottom: 16,
    marginLeft: 16,
    marginRight: 16,
    justifyContent: "center",
  },
  description: {
    fontSize: 11,
    fontStyle: "italic",
  },
  photo: {
    height: 200,
    width: "100%",
  },
});

export default CustomRow;
