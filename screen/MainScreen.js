import React, { useState, useEffect } from "react";
import { StyleSheet, View, TextInput, SafeAreaView } from "react-native";
import CustomListview from "./CustomListView";

function MainScreen({ navigation }) {
  const [tags, setTags] = useState("");
  const [page, setPage] = useState("1");
  const [first, setFirst] = useState(true);
  const [pictures, setPictures] = useState([]);

  useEffect(() => {
    getFlickrPictures();
  }, [page]);

  getFlickrPictures = async () => {
    var details = {
      tags: tags,
      page: page,
    };

    var formBody = [];
    for (var property in details) {
      var encodedKey = encodeURIComponent(property);
      var encodedValue = encodeURIComponent(details[property]);
      formBody.push(encodedKey + "=" + encodedValue);
    }
    formBody = formBody.join("&");

    fetch("https://aia-backend-test.herokuapp.com/picture", {
      method: "POST",
      headers: {
        "Content-Type": "application/x-www-form-urlencoded;charset=UTF-8",
      },
      body: formBody,
    })
      .then((res) => res.json())
      .then((json) => {
        if (page > 1) {
          setPictures(pictures.concat(json.items));
        } else {
          setPictures(json.items);
        }
      });
  };

  if (first) {
    getFlickrPictures();
    setFirst(false);
  }

  return (
    <SafeAreaView style={styles.container}>
      <View style={styles.container_search}>
        <TextInput
          placeholder="Search"
          style={styles.editText}
          onChangeText={(val) => setTags(val)}
          onSubmitEditing={() => {
            setPage("1");
          }}
        />
      </View>
      <CustomListview
        itemList={pictures}
        navigation={navigation}
        getFlickrPictures={getFlickrPictures}
        setPage={setPage}
      />
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    width: "100%",
  },
  editText: {
    height: 40,
    paddingLeft: 16,
    paddingRight: 16,
    paddingTop: 8,
    paddingBottom: 8,
    borderBottomWidth: 1,
    borderBottomColor: "#CECECE",
  },
});

export default MainScreen;
