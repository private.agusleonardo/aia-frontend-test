import React from "react";
import { StyleSheet, SafeAreaView } from "react-native";
import WebView from "react-native-webview";

function WebViewScreen({ route }) {
  const { flickr_url } = route.params;
  return (
    <SafeAreaView style={styles.container}>
      <WebView
        source={{
          uri: flickr_url,
        }}
      />
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    width: "100%",
  },
});

export default WebViewScreen;
